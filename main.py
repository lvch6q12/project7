from flask import Flask, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_migrate import Migrate

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://admin:admin@localhost:5432/project7'
app.secret_key = "HUYPIZDASATANA"
db = SQLAlchemy(app)
migrate = Migrate(app, db)

login_manager = LoginManager()
login_manager.init_app(app)
from project7.models import *

from project7 import routes


@login_manager.user_loader
def load_user(user_login):
    return User.query.filter_by(login=user_login).first()


@login_manager.unauthorized_handler
def unauthorized():
    return redirect(url_for('login'))
