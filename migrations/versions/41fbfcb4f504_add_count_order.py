"""add_count_order

Revision ID: 41fbfcb4f504
Revises: 05fc3343e1c7
Create Date: 2020-06-09 22:08:50.099609

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '41fbfcb4f504'
down_revision = '05fc3343e1c7'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('order', sa.Column('count', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('order', 'count')
    # ### end Alembic commands ###
