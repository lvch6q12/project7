from project7.main import db


class Basket(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    menu_id = db.Column(db.Integer, db.ForeignKey('menu.id'))
    sale = db.Column(db.Integer)
    date = db.Column(db.DateTime)
    count = db.Column(db.Integer, default=1)
