from project7.main import db


class Menu(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))
    sale = db.Column(db.Integer)
    category = db.Column(db.String(20))
    baskets = db.relationship('Basket', backref='menu', lazy=True)
    order_items = db.relationship('OrderItem', backref='menu', lazy=True)
