from project7.main import db


class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    date = db.Column(db.DateTime)
    status = db.Column(db.Boolean, default=False)
    items = db.relationship('OrderItem', backref='order', lazy=True)
