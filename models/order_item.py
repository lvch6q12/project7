from project7.main import db


class OrderItem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    menu_id = db.Column(db.Integer, db.ForeignKey('menu.id'))
    sale = db.Column(db.Integer)
    count = db.Column(db.Integer)
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'))
