from project7.main import db
from flask_login import current_user


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20))
    salary = db.Column(db.Integer)
    login = db.Column(db.String(20), unique=True)
    password = db.Column(db.String(20))
    type = db.Column(db.String(20))
    baskets = db.relationship('Basket', backref='user', lazy=True)
    orders = db.relationship('Order', backref='user', lazy=True)

    @property
    def is_active(self):
        return bool(self.password)

    @property
    def is_authenticated(self):
        return getattr(current_user, 'login', None) == self.login

    def get_id(self):
        return self.login
