from project7.main import app, db
from flask import render_template, request, redirect, url_for
from project7.models import Menu, User, Order, Basket, OrderItem
from flask_login import current_user, login_required, login_user, logout_user
from datetime import datetime
ADMIN = 'Администратор'
SELLER = 'Продавец'
USER = 'Пользователь'


@app.route('/')
def main():
    return render_template('hello.html')


@app.route('/users')
@login_required
def users_list():
    if current_user.type == SELLER:
        return redirect(url_for('main'))
    user_sql = User.query.all()
    return render_template('users.html', users=user_sql, login_user=current_user.type == ADMIN)


@app.route('/users/add', methods=['GET', 'POST'])
@login_required
def user_add():
    if not current_user.type == ADMIN:
        return redirect(url_for('login'))
    if request.method == 'POST':
        user = User(name=request.form['name'],
                    salary=request.form['salary'],
                    login=request.form['login'],
                    password=request.form['password'],
                    type=request.form['type'])
        db.session.add(user)
        db.session.commit()

        return redirect(url_for('users_list'))
    else:
        return render_template('user_add.html')


@app.route('/users/<id>/delete')
@login_required
def user_delete(id):
    if not current_user.type == ADMIN:
        return redirect(url_for('login'))
    user = User.query.filter_by(id=id).first()
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('users_list'))


@app.route('/menu')
@login_required
def list_menu():
    if current_user.type == SELLER:
        return redirect(url_for('main'))
    menu_sql = Menu.query.all()
    return render_template('menu.html', menu=menu_sql, login_user=current_user.type == ADMIN)


@app.route('/menu/add', methods=['GET', 'POST'])
@login_required
def menu_add():
    if not current_user.type == ADMIN:
        return redirect(url_for('list_menu'))
    if request.method == 'POST':
        menu = Menu(name=request.form['name'],
                    sale=request.form['sale'],
                    category=request.form['category'])
        db.session.add(menu)
        db.session.commit()
        return redirect(url_for('list_menu'))
    else:
        menu_sql = Menu.query.all()
        return render_template('menu_add.html', menu=menu_sql)


@app.route('/menu/<id>/delete')
@login_required
def menu_delete(id):
    if not current_user.type == ADMIN:
        return redirect(url_for('main'))
    menu = Menu.query.filter_by(id=id).first()
    db.session.delete(menu)
    db.session.commit()
    return redirect(url_for('list_menu'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if getattr(current_user, 'login', None):
        return redirect(url_for('main'))

    if request.method == 'POST':
        user = User.query.filter_by(login=request.form['login']).filter_by(
            password=request.form['password']).first()
        if user:
            login_user(user, remember=True)
            return redirect(url_for('main'))

        return render_template('login.html')

    else:
        return render_template('login.html')


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/basket/<id>', methods=['GET', 'POST'])
@login_required
def basket_add(id):
    if current_user.type == SELLER:
        return redirect(url_for('main'))
    buf_date = datetime.now()
    buf_basket = Basket.query.filter_by(menu_id=id).filter_by(user_id=current_user.id).first()

    if buf_basket:
        buf_basket.count += 1

    else:
        buf_menu = Menu.query.filter_by(id=id).first()
        basket = Basket(user_id=current_user.id,
                        menu_id=id,
                        sale=buf_menu.sale,
                        date=buf_date)
        db.session.add(basket)
    db.session.commit()

    return redirect(url_for('list_menu'))


@app.route('/basket')
@login_required
def basket():
    if current_user.type == SELLER:
        return redirect(url_for('main'))
    basket_sql = Basket.query.filter_by(user_id=current_user.id).all()
    return render_template('basket.html', basket=basket_sql, login_user=current_user.type == ADMIN)


@app.route('/basket/<id>/delete')
@login_required
def basket_del(id):
    if not current_user.type == ADMIN:
        return redirect(url_for('main'))
    del_basket = Basket.query.filter_by(id=id).first()
    db.session.delete(del_basket)
    db.session.commit()
    return redirect(url_for('basket'))


@app.route('/order/add', methods=['GET', 'POST'])
@login_required
def order_add():
    if current_user.type == SELLER:
        return redirect(url_for('main'))
    buf_date = datetime.now()
    if request.method == 'POST':
        buf_basket = Basket.query.filter_by(user_id=current_user.id).all()
        order = Order(user_id=current_user.id,
                      date=buf_date)
        db.session.add(order)
        db.session.commit()
        for item in buf_basket:
            order_item = OrderItem(menu_id=item.menu_id,
                                   count=item.count,
                                   sale=item.sale,
                                   order_id=order.id
                                   )
            db.session.add(order_item)
            db.session.delete(item)
            db.session.commit()
        return redirect(url_for('order'))


@app.route('/order')
@login_required
def order():
    if current_user.type == USER:
        buf_order = (Order.query.filter_by(user_id=current_user.id)
                                .order_by(Order.id)
                                .order_by(Order.date).all())
    else:
        buf_order = (Order.query.order_by(Order.id)
                                .order_by(Order.date).all())

    return render_template('order.html', orders=buf_order, login_user=current_user.type)


# @app.route('/order_items')
# @login_required
# def order_items():
#     buf_order_items = OrderItem.query.all()
#     return render_template('order_items.html',
#                            order_items=buf_order_items,
#                            login_user_seller=current_user.type == SELLER,
#                            login_user_admin=current_user.type == ADMIN)


@app.route('/show_order/<order_id>')
@login_required
def show_order(order_id):
    if current_user.type == USER:
        order = Order.query.filter_by(id=order_id).filter_by(user_id=current_user.id).first()
        if order:
            buf_order = OrderItem.query.filter_by(order_id=order_id).all()
            return render_template('show_order.html', orders=buf_order, login_user=current_user.type == ADMIN)
        else:
            return redirect(url_for('main'))
    else:
        buf_order = OrderItem.query.filter_by(order_id=order_id).all()
        return render_template('show_order.html', orders=buf_order, login_user=current_user.type == ADMIN)


@app.route('/order_status/<order_id>', methods=['GET', 'POST'])
@login_required
def order_status(order_id):
    if current_user.type == ADMIN or current_user.type == SELLER:
        if request.method == 'POST':
            buf_order = Order.query.filter_by(id=order_id).first()
            buf_order.status = True
            db.session.commit()
            return redirect(url_for('order'))
    else:
        return redirect(url_for('main'))
